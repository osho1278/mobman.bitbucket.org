XSS (Cross Site Scripting)
==========================

If you dont know about cross site scripting, then please go to OWASP's website and get an idea on what it is and then come back here !!!

How to Run this module 
----------------------

on your linux terminal 
 - "cd into the xss project directory"
 - type "python directory_read.py 5 <complete path to your PHP project>"
	- Here 5 indicates the depth to which you have your PHP files
 - hit enter to get the list of vulnerabilities

Functions in FILES (in the order of usage)
------------------------------------------
- `directory_read.py`_
- `xss.py`_ xss_check() 
- `graph.py`_ make graph() 
- `get_all_variables.py`_ getvariable() 
- `operations_statements.py`_ expressions() 
- `get_dict.py`_ get_dict() 
- `recursive_check.py`_ rec() 
- `is_secure_before_use.py`_  is_secure_before_use() 
- `dev.html`_
- `report.html`_

directory_read.py
.................
This file is the heart of all the XSS module. This file calls all the functions listed above in `Functions in FILES (in the order of usage)`_

xss.py
......
this file calls the "xss_check()" method , which returns a string of the form
::
	['\$name','echo',259],['\$name','echo',179],['\$say','echo',168],['\$ggg','echo',150],['\$say','$a'],['\$say','$b']

this means that $name variables is echoed at position 259 and 179 respectively and so on.


graph.py
........
This file contains the "make graph()" method which is responsible for plotting the graphs using python libraries.
the string given to this files is the return value of "xss_check()" from `xss.py`_ which in the above case would be
::
	['\$name','echo',259],['\$name','echo',179],['\$say','echo',168],['\$ggg','echo',150],['\$say','$a'],['\$say','$b']

Graphs made are stored corresponding to their file names in pics directory inside the xss project directory.


get_all_variables.py
....................
this file contains the "getvariable()" method. this method takes in a file name as argument and returns an string of the form
::
	['$name','$a'],['$name','$r'],['$say','$b'],['$a','$_POSTname']
this basically means how variables are related to each other.


operations_statements.py
........................

this file contains the "expressions()" method. this method takes in a file name as argument and returns an string of the form
::
	['$name','$a'],['$name','$r'],['$say','$b'],['$a','$_POSTname'],['\$say','$a'],['\$say','$b']

Difference between getvariable and expressions method :
getvaribale will only return single level relation ship. for example :
::
	$a=$b;
	$b=$c+$d;
here getvariable will return only
::
	[$a,$b]
whereas expressions will return
::
	[$b,$c],[$b,$d]
get_dict.py
...........
this file calls the "get_dict()" method. It takes a string as argument which is sum of retun values from getvaribale and expressions
and returns
::
	Input-----['$name','$a'],['$name','$r'],['$say','$b'],['$a','$_POSTname'],['\$say','$a'],['\$say','$b']
 
	Output----{'$name': ['$a', '$r'], '$say': ['$b', '$a'], '$a': ['$_POSTname']}


recursive_check.py
..................
rec()


is_secure_before_use.py
.......................
This is where the actual magic happens. The "is_secure_before_use()" method does what is needed to check xss vulnerability.
This function take the following arguments 
::
	- dd = dictionary as described in `get_dict.py`_
	- temp1 = it is the varibale to be checked
	- x = is the location where temp1 first occured
	- ss = data of the file (that is the PHP code ), without any line breaks.
	- path = path of the file being scanned
	- till = it is the position till where the "ss" above will be taken



This file contains many other methods too:
::
	- get_valid_dict = This method will return a dictionary after removing the keys and values that do not occur in ss, when ss has been taken till "till".
	- remove_redundancy(dd) = if an element is a key and a value too, then it has to be removed . this will avoid infinite loop. for example : {$a:[$a,$b]}.
				now if each time i replace "$a" with "$a,$b" , this will go to an infinite loop. hence we have to avoid all such occurances. 
				hence this method returns back the dictionary after removing all the discrepancies.		
	- remove_useless_keys(dd,liss) =
	- secure_check(temp11,x,var[i],ss) = this method checks if a variable temp11 is secure or not.
	- end(temp11,var[i],ss) = a string is formed using "temp11" and "var[i]" and its position in ss is calculated and returned.
	- recursive(nn,var[i]) = for a element being scanned, if it is not secure , then this method is called. if that particular element is a key in the 
				dictionary then it is replaced by the corresponding values.


dev.html
........
This file is created dynamically in directory_read.py and 

This is also a graph but this is a hierarchy graph made by using google API.
this graph did not included all the variables hence it has been given less importance after the developemnt of `graph.py`_ file.



report.html
...........
This file is created dynamically in directory_read.py

To view the reports in HTML format and to go to the images directly this module is created. This is just to simplify the design process.

