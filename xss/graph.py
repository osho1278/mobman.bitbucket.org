#!/usr/bin/env python

# Copyright (c) 2007-2008 Pedro Matiello <pmatiello@gmail.com>
# License: MIT (see COPYING file)

# Import graphviz
from remove_unwanted import remove_unwanted
import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')
import gv

# Import pygraph
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.readwrite.dot import write
import re
from remove_unwanted import remove_unwanted
# Graph creation

def make_graph(lol,file):
	gr = graph()
	lis=[]
	str1=re.findall(r'\[(.*?)\]',lol)
	# print "lauda lehsun",str1
	# print lol
	for xx in str1:
		li=xx.split(",")
		# print "li=====",li[1]
		li[1]=remove_unwanted(li[1])
		if(li[1]):
			li[0]=remove_unwanted(li[0])
			li[1]=remove_unwanted(li[1])
			# print li
			if (li[0] not in lis):
				lis.insert(0,li[0])
			if (li[1] not in lis):
				lis.insert(0,li[1])
		# gr.add_edge((li[0], li[1]))
	# print lis
	gr.add_nodes(lis)
	# Add nodes and edges
	# gr.add_nodes(["Portugal","Spain","France","Germany","Belgium","Netherlands","Italy","Switzerland","Austria","Denmark","Poland","Czech Republic","Slovakia","Hungary"])
	# gr.add_nodes(["echo","$name","$ggg","$a","$b"])
	temp=[]
	for xx in str1:
		li=xx.split(",")
		
		li[0]=remove_unwanted(li[0])
		li[1]=remove_unwanted(li[1])
		# print li
		if(li[1] and li[0] not in temp):
			temp.insert(0,li[0])
			gr.add_edge((li[0], li[1]))

	# print "gr====",gr
	if(gr):
		# print "plotting graph"
		st, order = breadth_first_search(gr)
		gst = digraph()
		gst.add_spanning_tree(st)

		dot = write(gst)
		gvv = gv.readstring(dot)

		gv.layout(gvv,'dot')
		name1=file.split("/")
		name=name1[0]+".png"
		z=len(name1)
		# print name1[z-1]
		name1=name1[z-1].split(".")
		name=name1[0]+".png"
		
		gv.render(gvv,'png',"pics/"+name)
		# raw_input()
	# Draw as PNG
	# dot = write(gr)
	# gvv = gv.readstring(dot)
	# gv.layout(gvv,'dot')
	# gv.render(gvv,'png','europe.png')